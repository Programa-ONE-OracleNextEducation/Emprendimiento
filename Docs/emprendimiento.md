## **Realice este curso para Startups y Emprendimiento y:**

- Entiende qué es emprender
- Conozca la mentalidad emprendedora
- Descubra si es posible emprender como empleado de la empresa
- Construya tu modelo de negocio
- Crea tu plan de negocios
- Valide una idea de negocio
- Verifique el atractivo del mercado

### **Aulas**

- **Pilares del emprendimiento**

  - Presentación
  - Sobre emprendimiento
  - Emprendimiento corporativo
  - Intraemprendimiento
  - Los 3 pilares
  - Mindset Empreendedor
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Ideas y oportunidades**

  - Ideas vs oportunidades
  - Atractividad de mercado
  - Escalabilidad
  - Posicionamento
  - Atractividad
  - Haga lo que hicimos en aula
  - Lo que aprendimos en aula

- **Modelo de negocios**

  - Propuesta de valor
  - Tecnologías, operaciones y márgenes
  - Complemento
  - Obstáculos
  - Operaciones
  - Haga lo que hicimos en aula
  - Lo que aprendimos en aula

- **Business Model Canvas**

  - Business Model Canvas
  - Business Model Canvas
  - Canvas Proposta de valor
  - Haga lo que hicimos en aula
  - Lo que aprendimos en aula

- **Plan de negocios**

  - Plan, sumário, producto y servicio
  - Beneficios de hacer un plan de negocios
  - Time, mercado, marketing y ventas
  - Cronograma, riesgos y finanzas
  - Riesgos
  - Haga lo que hicimos en aula
  - Lo que aprendimos
  - Conclusión
