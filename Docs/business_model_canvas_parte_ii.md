## **Realice este curso para Transformación Ágil y:**

- Presenta tu idea a un inversionista con un plan de negocio estructurado
- Entender y cuestionar las limitaciones de cualquier idea de negocio identificando sus principales recursos
- Identifica posibles socios y ahorra en la estructura de tu negocio
- Estructura e identifica aquellos los costos de tu modelo de negocio y ofrece más valor a tus clientes
- Itera y replantea constantemente tu empresa para resolver problemas de forma innovadora
- Utiliza un modelo de negocio didáctico y potente que te ayude a pensar y cuestionar tu idea
- Transforma tu idea en el modelo ideal de forma asertiva y rápida
- Empezar una startup de forma rápida, sencilla y visual en 9 pasos

- **Aulas**

- **Fuente de Ingresos**

  - Presentación
  - Introducción al Business Model Canvas II
  - Fuente de Ingresos: Introducción
  - Fuente de Ingresos
  - Cuota de Suscripción y Cuota por Uso
  - Caso: Disney+ y Deezer
  - Caso: Operadores de telefonía móvil
  - Venta de activos y licencias
  - Caso: Fabricante de vehículos
  - Caso: Microsoft Office
  - Préstamo, alquiler y anuncios
  - Caso: Inmobiliarias
  - Tasa o gastos de corretaje
  - Caso: YouTube
  - Caso: MercadoPago
  - Fuente de Ingresos: Resumen
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Recursos Clave**

  - Recursos Clave: Introducción
  - Recursos Clave
  - Recurso humano y físico
  - Caso: Abriendo nuestra propia empresa
  - Caso: Alura y Amazon
  - Intelectuales y económicos
  - Caso: Una editorial
  - Un recurso indispensable
  - Recursos Clave: Resumen
  - Tipos de Recursos Clave
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Actividades Clave**

  - Actividades Clave: Introducción
  - Actividades Clave
  - Tipos de Actividades Clave
  - Caso: Fabricantes de celulares
  - Caso: Consultoría en Experiencia de Usuario (UX)
  - Caso: Aplicación Me Transporte
  - Actividades Clave: Resumen
  - Tipos de Actividades Clave
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Asociaciones Clave**

  - Identificando Asociaciones Clave
  - Asociaciones Clave
  - Recursos y actividades particulares
  - Caso: Aplicaciones de movilidad urbana
  - Optimización y economía de escala
  - Caso: Fabricantes de bicicletas
  - Reducción de Riesgos e Incertidumbre
  - Caso: Bike Itaú y Muvo
  - Asociaciones Clave: Resumen
  - Tipos de Asociaciones Clave
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Estructura de Costos**

  - Intro: Estructura de Costos
  - Estructura de Costos
  - Costos Variables
  - Caso: Conductores de Didi
  - Costos Fijos y Determinados por Costos
  - Caso: Los salarios
  - Caso: Carro compartido
  - Por el Valor y Economía de Escala
  - Caso: Hoteles de Lujo
  - Caso: Cuponatic
  - Economías de Campo
  - Caso: Disminuyendo el equipo
  - Resumen - Estructura de Costos
  - Tipos de estructuras de Costos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Próximos pasos**

  - Pasos siguientes
  - Otros tipos de Canvas
  - Haga lo que hicimos en aula
  - Lo que aprendimos
  - Finalizando
