## **Realice este curso para Startups y Emprendimiento y:**

- Crea tu MVP: Producto Mínimo Viable
- Estudia los casos de Google Wave, Dropbox, entre otros
- Pivotea lo que no está funcionando
- Conozca el Ciclo de validación de tu idea
- Empieza con la metodologia LEAN

### **Aulas**

- **Presentación**

  - Presentación
  - Preparando el ambiente

- **Experimentación rápida y aprendizaje validado**

  - Experimentación rápida y aprendizaje validado
  - Lean Startup
  - Early adopters
  - Ciclo de validación
  - Lo que aprendimos

- **Fases del MVP**

  - Etapas del MVP
  - Producto Minimo Viable (MVP)
  - Etapas del MVP
  - Precificación
  - Último paso del MVP
  - Lo que aprendimos

- **Medir y pivotar**

  - Medir y pivotar
  - Pivotes
  - Tipos de pivotes
  - Lo que aprendimos

- **Acelerar, crecer e inovar**

  - Acelerar, crecer e innovar
  - Métricas
  - Agrupar en lotes
  - Motores de crecimiento
  - Lo que aprendimos

- **Recomendaciones para prototipar**

  - Recomendaciones para prototipar
  - Hipótesis de negocio
  - Prueba A/B
  - Lo que aprendimos

- **Finalización**

  - Beneficios de Lean
  - Lo que aprendimos
  - Conclusión
