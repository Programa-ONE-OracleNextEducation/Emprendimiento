## **Realice este curso para Startups y Emprendimiento y:**

- Conozca las técnicas de presentaciones rápidas
- Descubra cómo impactar positivamente sus resultados con un discurso
- Comprende lo que las personas que te escuchan realmente quieren escuchar
- Aprende a llamar la atención del entrevistador
- Disfruta cada segundo de una conversación
- La técnica Pitch para no tener que preocuparse
- Escribe tu Business Model You

### **Aulas**

- **Presentando el Pitch**

  - Presentación
  - Introducción al pitch
  - Simon Sinek
  - Presentación de Eliana
  - Preparación
  - Pitch para aprendiz
  - Haz lo que hicimos
  - Lo que aprendimos

- **Creando un discurso**

  - Business Model You
  - Business Model You
  - Que es el BMY
  - Relleno de plantilla
  - Detalles
  - Haz lo que hicimos
  - Lo que aprendimos

- **Escribe tu pitch**

  - Los primeros Pitches
  - Transformando BMY en Pitch
  - Los Pitches intermedios
  - Haz lo que hicimos
  - Lo que aprendimos

- **Analizando el pitch**

  - Mejorando su pitch
  - Pitchs mejorados
  - ¿Cómo mejorar tu Pitch?
  - Haz lo que hicimos
  - Lo que aprendimos

- **De qué debemos preocuparnos**

  - Cuidados
  - Becas
  - Para saber más
  - Haz lo que hicimos en aula
  - Lo que aprendimos
  - Conclusion
