## **Realice este curso para Transformación Ágil y:**

- Materializa la idea de negocio que tienes en mente y ponla en práctica
- Presenta tu idea a un inversor con un plan de negocio estructurado
- Utiliza un modelo de negocio didáctico y potente que te ayude a pensar y cuestionar tu idea
- Transforma tu idea en el modelo ideal de forma asertiva y rápida
- Empezar una startup de forma rápida, sencilla y visual en 9 pasos

### **Aulas**

- **Brief**

  - Presentación
  - Preparando el ambiente
  - Brief - Entendiendo el negocio
  - Entendimiento del negocio
  - Problemas del Brief
  - Introducción al Business Model Canvas
  - Validando ideas de negocio
  - Factores externos
  - Estudiando la competencia
  - Acertando en mi idea de negocio
  - Modelo de negocio
  - Áreas del modelo de negocio
  - Haga lo que hicimos en aula

- **Segmento de clientes**

  - Segmento de clientes #1 - Los conductores
  - Segmento de clientes #2 - Los pasajeros
  - Entendiendo el segmento de clientes
  - Identificando el segmento de clientes
  - Tipos de Mercado #1
  - Tipos de Mercado #2
  - Tipos de Mercado
  - Hipótesis sobre el segmento de clientes
  - Entendiendo el segmento de clientes
  - Resumiendo el Canvas
  - Segmento de Mercado - App Me Transporte
  - Segmento de Mercado - Restaurantes
  - Segmento de Mercado - Tinder
  - Segmento de Mercado - Bancos
  - Haga lo que hicimos en aula
  - Lo que aprendimos: Como va nuestro Canvas

- **Propuesta de valor**

  - Propuesta de valor
  - Acerca de la propuesta de valor
  - Tipos de propuesta de valor
  - Novedad y estatus
  - Caso Snapchat
  - Caso Rolex y Ferrari
  - Reducción de riesgo, conveniencia y usabilidad
  - Compra de un vehículo usado
  - Caso del iPod y el iPad
  - Accesibilidad y desempeño
  - Caso Netflix
  - Computadores para Gamers
  - El trabajo hecho, personalización y reducción de costos
  - Caso PayPal
  - Caso Uber
  - Identificando la propuesta de valor
  - Resumen de la propuesta de valor
  - Haga lo que hicimos en aula
  - Lo que aprendimos: Cómo va nuestro Canvas - Propuesta de valor

- **Canales**

  - Introducción a los Canales
  - Canales
  - Utilizando Facebook
  - Caso Facebook
  - Tipos de Canales
  - Varios tipos de Canales
  - Creando un sitio web - Canal directo
  - Canales propios directos
  - Resumiendo el Canvas - Canales
  - Haga lo que hicimos en aula
  - Lo que aprendimos: Resumen de los Canales

- **Relación con los clientes**

  - Introducción - Relación con clientes
  - Sobre la relación con los clientes
  - Asistencia personal y servicios automáticos
  - Relación con los clientes - Me transporte
  - Relación con los clientes - Nubank
  - Asistencia personal dedicada y co-creación
  - Relación con los clientes - Adobe
  - Relación con los clientes - Youtube y TikTok
  - Comunidades y autoservicio
  - Relación con los clientes - Alura
  - Relación con los clientes - Facebook
  - Tipos de relación con los clientes
  - Identificando la relación con clientes
  - Resumiendo la relación con clientes
  - Haga lo que hicimos en aula
  - Lo que aprendimos: Resumen de la relación con los clientes
  - Resumen del curso
