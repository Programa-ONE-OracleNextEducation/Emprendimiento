# **Pitch para entrevistas - Haga presentaciones impactantes**

## **Presentando el Pitch**

### **Simon Sinek**

Aprende más sobre el círculo dorado de [Simon Sinek](https://rockcontent.com/es/blog/simon-sinek/ "Simon Sinek"), con el objetivo de invertir la forma tradicional de presentar un concepto, idea o producto.

Haz [clic aquí](https://www.youtube.com/watch?v=obheq9VA5KI "clic aquí") y ve la conferencia más conocida que fue parte de las charlas de TED (impartida por Simon Sinek): Cómo los grandes líderes inspiran la acción.

### **Pitch para aprendiz**

Para obtener más información sobre cómo grabar un Pitch para procesos de selección de Trainee (aprendiz), haz [clic aquí](https://youtu.be/2b3xG_YjgvI "clic aquí") y ve varios ejemplos de Pitch para empresas como Heinz, Burger King e incluso Ambev.

### **Haz lo que hicimos**

¿Alguna vez has tenido una entrevista de trabajo o has tenido que realizar una presentación sobre un producto / servicio tuyo a un cliente?

Ten en cuenta qué tipo de presentación vas a enfrentar y responde:

- ¿Cuál es el objetivo final de esta presentación?
- ¿A quién va dirigida esta presentación?
- ¿Alguna vez has tenido que presentarte a alguien?

### **Lo que aprendimos**

Cada día que pasa, el tiempo se vuelve más escaso y, como resultado, las personas no tienen tiempo para hablar entre sí, por lo que es importante que haya presentaciones rápidas en el mundo de los negocios que sean impactantes, y esto ha cambiado cada vez nuestro mercado.

Esta presentación debe ser:

- Rápida.
- Impactante.
- Con significado.

Los primeros segundos de cualquier presentación son importantes porque:

Si se logra captar la atención del público, este estará atento al discurso para obtener más información, caso contrario, ignorará automáticamente dicha información.

Los discursos no solo se usan para la participación política y social, sino que también los usamos para vender ideas y negocios.

**¿Cómo se produce Pitch?**

En la década de 1990, Michael Caruso (editor en jefe de Vanity Fair) se reunió con su jefe todas las mañanas en el ascensor y llegó a la conclusión de que ese tiempo no tenía que desperdiciarse. Así que todos los días organizaba las pautas que serían discutidas en la conversación con su jefe durante el tiempo en el ascensor.

Utilizada en los procesos de selección. El currículum de video (Pitch) es la nueva tendencia de las selecciones de aprendices. Este método se puede usar tanto en las ruedas de proceso de selección como en videos en Internet.

**Preparación del Pitch para procesos de selección:**

- Habla objetivamente sobre quién eres.
- Cuenta tu historia brevemente.
- Explica cómo puedes agregar valor a la empresa.

**Precauciones de grabación de video:**

- Revisa el fondo.
- Revisa el sonido.
- Se cuidadoso al escribir el guión (o texto).
- Cuida tu apariencia.

---

## **Creando un discurso**

### **Business Model You**

Business Model You disponible para descargar:

![Business Model You](img/business_model_you.png "Business Model You")

### **Que es el BMY**

**¿Qué es el "Business Model You"?**

Una herramienta creada para facilitar la dificultad que tienen las personas para hablar de sí mismas. El "Business Model You" tiene el formato idéntico a Canvas (herramienta utilizada para estructurar un modelo de negocio) pero las preguntas son diferentes.

Para conocer más, échale un [vistazo](http://www.santigarcia.net/2016/10/business-model-you-una-alternativa-las.html#:~:text=Aparte%20de%20ayudar%20a%20que,para%20el%20alineamiento%20de%20la "vistazo").

### **Haz lo que hicimos**

De acuerdo con la herramienta "Modelo de Negocio Tú", responde las siguientes preguntas:

- ¿Quién te ayudó a ser quién eres hoy?
- ¿Qué sabes hacer?
- ¿Quién eres y qué tienes para ofrecer?
- ¿Cómo contribuirás a quién te contrate?
- ¿Cómo interactúas?
- ¿Cómo te conoce la gente?

Cuando tengas las respuestas de tu "Business Model You", transfórmalas en un guión o texto.

### **Lo que aprendimos**

**¿ Qué es el "Business Model You"?**

Una herramienta creada con el objetivo de superar la dificultad que las personas tiene de hablar o de escribir sobre si mismas. El "Modelo de Negocio Tú" tiene un formato idéntico al Canvas (Herramienta utilizada para estructurar tu Modelo de Negocio) sin embargo, las preguntas son diferentes.

![Modelo de negocio personal](img/modelo_de_negocio_personal.png "Modelo de negocio personal")

[Fuente](https://josezamalloaj.wordpress.com/2017/09/08/el-inicio-ideas-y-herramientas-del-personal-branding/ "Fuente")

---

## **Escribe tu pitch**

### **Haz lo que hicimos**

En esta clase vimos como Beatriz convirtió las respuestas de su "Business Model You" en un texto.

Ahora puedes hacer lo mismo con el tuyo.

Puedes ver la presentación de Beatriz abriendo el archivo : Beatriz_Pitch_2.pdf

Esta presentación tiene algunos puntos importantes:

- Nombre y edad.
- Formación académica.
- Principales habilidades de comportamiento.
- Contribución a empresas en las que ya has trabajado.

### **Lo que aprendimos**

En esta clase, aprendimos como llenar el “Business Model You”, y como puede ser transformado en un Pitch.

¿Qué pasos necesitamos para transformar el “Business Model You” en una presentación de Pitch?

- Completa las preguntas del Business Model You.
- Adapta tus respuestas en formato de texto.
- Estudia el resumen.
- Practica al frente de la cámara.
- Al momento de grabar, habla con clareza y precisión.

---

## **Analizando el pitch**

### **¿Cómo mejorar tu Pitch?**

Lucas está haciendo un pitch para su proceso de selección de aprendices, ya ha respondido las preguntas del “Business Model You" y ha organizado toda la información en forma de texto. Pero su guión no es exactamente lo que quería, todavía falta algo.

¿Cómo puede Lucas mejorar su guión con la información previamente definida?

- Lucas debe organizar la información subrayada en el primer párrafo. Correcto, por lo que Lucas priorizará la información más importante e impactante en sus primeras líneas de Pitch.
- Con el texto en la mano, Lucas debe enfatizar las partes que considera más importantes. Correcto, entonces Lucas resaltará su información más importante e impactante.
- Lucas debe organizar la información que no está resaltada en forma cronológica. Correcto, de esa manera Lucas creará una historia / guión para su presentación.

### **Haz lo que hicimos**

Refinar tu presentación es un paso importante, ya que debes prestar atención al tiempo que tienes para hablar y dar prioridad a la información más importante sobre ti.

Con eso en mente, realiza estos ajustes en tu material para hacerlo más objetivo y claro.

### **Lo que aprendimos**

En esta clase, aprendimos la mejor manera de completar, organizar y mejorar el "Business Model You" con la información previamente definida.

¿Cómo mejorar tu Pitch?

- Es importante que resaltes las partes que consideras más importantes en tu guión (o texto).
- Organiza la información más impactante en el primer párrafo.
- En los siguientes párrafos organiza el resto de la información cronológicamente.

---

## **De que debemos preocuparnos**

### **Becas**

Patrícia fue invitada a participar en un programa de becas en Alura y la primera fase de este proceso de selección es presentar un Pitch en video.

Para que Patrícia pase la primera fase, ¿qué acciones debería tomar?

Rta.

Patrícia debería tener BMY como base para su guión, preocuparse por la apariencia y la vestimenta, la calidad del video en general y hablar con claridad y precisión. Correcto, si Patrícia pone en práctica todos estos requisitos, tendrá una mejor oportunidad de pasar la primera fase del proceso de selección.

### **Para saber más**

Un consejo para ti, en caso de que vayas a grabar el Pitch en tu teléfono:

[BIGVU](https://bigvu.tv "BIGVU") es una aplicación diseñada para funcionar como un estudio móvil, combina una plataforma para grabar, gestionar y publicar videos con un teleprompter, por lo tanto, no es necesario memorizar líneas ni mirar hacia abajo, el texto aparece en la pantalla de tu teléfono celular mientras estás siendo grabado y puedes regular la velocidad a la que avanza.

### **Lo que aprendimos**

La presentación del Pitch puede ser en persona y en video:

Probablemente, el Pitch en persona se lleve a cabo en una dinámica de grupo en algún proceso de selección. En primer lugar, es importante que revises el texto y te dediques a la presentación, evita gesticular en exceso con las manos y asegúrate de no dar la espalda a nadie.

Si el Pitch es en video, presta atención al escenario y la ropa que usarás, sigue el guión de BMY y graba el video para que puedas usarlo en varios procesos de selección.
