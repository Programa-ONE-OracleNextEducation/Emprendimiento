# **Business Model Canvas parte I - Un modelo poderoso para tu negócio**

## **Brief**

### **Preparando el ambiente**

Para comenzar el curso

En este curso vamos a llenar el Business Model Canvas. Para eso, puedes ingresar en el link que tenemos aquí y guardar una copia del modelo. Estoy seguro que lo usarás en otros proyectos e ideas de negocio.

Podrás compartirlo posteriormente con amigos o futuros socios.

_Ver : Business_Model_Canvas_Template.pdf_

Adicionalmente, encontrarás en PDF la versión del Brief que mostraremos más adelante y resolveremos a lo largo del curso.

_Ver : Brief.pdf_

¡Listo! Con el Business Model Canvas y el Brief estás listo o lista para comenzar.

Aquí vamos.

### **Entendimiento del negocio**

Como vimos en la clase anterior, fue necesario hacer un brief presentando nuestro negocio. ¿Qué fue lo que nos motivó a hacerlo?

Rta.

El negocio presentó problemas que podían ser evitados y necesitaban ser corregidos. Los fundadores pensaban que la idea podría mejorar buscando un poco más. El negocio presentó problemas que podían ser evitados y era necesario corregirlos. Al profundizar un poco más en la búsqueda, se hubiera podido mejorar la idea.

### **Problemas del Brief**

¿Cómo podríamos evitar la cantidad de problemas que vimos en el Brief?

Rta.

Buscar más información de los competidores, entender cómo operan y comprender qué es lo que necesitan mis clientes. Buscar más información de mis competidores, entender cómo funcionan y comprender qué necesitan mis clientes es la mejor opción, ya que podemos aprender mucho con lo que ya existe en el mercado. No tenemos que "reinventar la rueda". Por otro lado, abrimos la posibilidad de nuevas alternativas para hacer lo que ellos ya hacen de forma diferente.

### **Validando ideas de negocio**

Luego de tener una idea de negocio, ¿qué podríamos hacer en una primera etapa para validar si los clientes están interesados en nuestra propuesta?

Rta.

Crear una página web con una pequeña descripción del producto o servicio. Si alguien tiene interés, puede dejar su nombre y correo electrónico para entrar en contacto a futuro. Validar la idea de negocio antes de hacer prototipos es la mejor opción. Podemos usar páginas como Launch Rock (que vimos en clase). Otra opción puede ser crear un formulario (en Google Forms o Microsoft Forms) y compartirlo en redes sociales con amigos y conocidos. Esto te dará una visión más profunda de la idea de negocio sin estar sesgada como tu opinión o la opinión de tus amigos.

### **Estudiando la competencia**

¿Cómo podría saber cómo mis competidores trabajan?

Rta.

- Buscando las páginas de los competidores en Facebook y ver qué opinan los clientes del producto o servicio que ellos prestan.
- Haciendo una lista de las funcionalidades que ofrecen mis competidores contra las funcionalidades que espero ofrecer para ver en qué puedo ser mejor.
- Haciendo un levantamiento de todas las funcionalidades que ofrece mi competidor.
- Utilizando el producto o servicio de mis competidores para entender qué es lo que hacen y cómo lo hacen.

Cuando estamos iniciando una empresa o idea de negocio, necesitamos entender muy bien lo que hacen nuestros competidores, incluyendo cómo ofrecen su servicio o fabrican el producto. Eso incluye varias tareas y actividades que cubren todas las opciones mencionadas en este ejercicio.

### **Acertando en mi idea de negocio**

¿Cómo puedo tener certeza de que estoy pensando en los puntos principales que debe considerar mi idea de negocio?

Rta.

Haciendo un modelo de negocios y escribiendo todo lo que debo saber para entender más sobre mi idea. Seguir un modelo de negocios puede llevarnos a concluir y cuestionar ideas que anteriormente pensábamos como importantes y primordiales antes de realizar la primera línea de código o los diseños iniciales. Sin ese entendimiento, podemos perder el foco de lo que se espera de nuestro negocio y asumimos ideas sin considerar toda la estructura que un emprendimiento necesita para que pueda funcionar correctamente.

### **Modelo de negocio**

¿Qué es un modelo de negocio?

Rta.

- Una guía para entender y cuestionar todos los puntos que determinan si una idea de negocio es viable o no.
- Una herramienta que permite visualizar si una idea de negocio tiene la estructura en todos los puntos que requiere.
- La forma en la que una empresa crea valor para sus clientes.
- Un estructurador del negocio.

### **Áreas del modelo de negocio**

¿Cuáles son las áreas principales de un modelo de negocio tradicional que cubre el Business Model Canvas?

Rta.

Infraestructura, clientes, viabilidad financiera y oferta. Los modelos de negocio tradicionales son complejos. El Business Model Canvas es una herramienta que te ayuda a entender y agiliza la construcción de un modelo de negocio. Las áreas que cubre el Business Model Canvas de un modelo de negocio tradicional son: Infraestructura, clientes, viabilidad financiera y oferta.

---

## **Segmento de clientes**

### **Entendiendo el segmento de clientes**

¿Cuál es el significado de la sección "Segmento de Clientes" del Business Model Canvas?

Rta.

Sección en la que identifico y entiendo para quién voy a vender el producto o el servicio. En el segmento de clientes identificamos y entendemos para quién vamos a vender el producto o servicio que estamos ofreciendo. Entender este punto es fundamental para el éxito o fracaso de un negocio según lo que vimos en la clase anterior.

### **Identificando el segmento de clientes**

¿Cómo puedo identificar cuál es el segmento de clientes correcto para mi negocio?

Rta.

Usando algunas estrategias como la búsqueda de información poblacional, creación de un formulario online para conocer la edad y región de los grupos de interés, validación del público que se verá beneficiado y tienen interés en el negocio, consulta de algunos artículos que estudien el comportamiento de ese grupo poblacional.

Los Institutos de investigación poblacional tienen millones de datos de uso gratuito para tener una visión general de la población. Estos datos tienen mayor confiabilidad. También es útil crear formularios online que nos permitan hacer preguntas específicas de los hábitos de consumo y comportamiento de esa población, leer artículos, etc.

Alternar esos métodos permite tener un entendimiento del segmento de clientes mucho más profundo y claro.

### **Tipos de Mercado**

¿Cuáles son los tipos de Mercado que podemos tener?

Rta.

Mercado de masa, mercado de nicho, mercado diversificado, mercado segmentado y mercado multilateral. Cuando hablamos de segmento de clientes, tenemos que buscar e identificar cual es el tipo de mercado de nuestro negocio. Como vimos en la clase, se pueden clasificar en Mercado de masa, mercado de nicho, mercado diversificado, mercado segmentado y mercado multilateral.

### **Hipótesis sobre el segmento de clientes**

Sobre el segmento de clientes NO es correcto decir que:

Rta.

Los segmentos de clientes nunca cambian. No podré cambiar mi segmento de clientes una vez decida a quién voy a ofrecer mi producto o servicio. Cuando hablamos de segmento de clientes en un mundo dinámico y cambiante, no podemos asegurar que siempre será el mismo. Los productos y servicios tienen ciclos de vida y en esos cambios, podemos ganar nuevos clientes y perder otros. Es importante entender que tenemos un cliente al que ofrecemos nuestro producto, pero otro segmento puede verlo interesante. No es algo fijo, sin embargo, nos enfocamos en determinada población para ofrecer nuestro mejor valor.

### **Entendiendo el segmento de clientes**

Cuando estamos llenando la sección de "segmento de clientes", ¿qué es lo que buscamos entender?

Rta.

Cuando completamos el cuadrante de los segmentos de clientes, tengo que investigar y comprender quiénes son y dónde están los clientes potenciales del negocio y entender que son completamente diferentes y variados.

Cuando completamos el cuadrante de segmentos de clientes, necesito investigar y entender quiénes son y dónde están los clientes potenciales que voy a tener y entender que son diferentes y variados. Sin embargo, este segmento de clientes puede variar según el ciclo de vida del producto y/o servicio. También podemos encontrar otro tipo de clientes que no identificamos inicialmente, pero que se identifican con la propuesta de valor de nuestro negocio.

### **Segmento de Mercado - App Me Transporte**

Cuando estábamos identificando cuál era el tipo de mercado de la App Me Transporte, identificamos que teníamos más de un segmento de clientes y que uno no podía existir sin el otro. ¿Cómo se llama este tipo de segmento de mercado?

Rta.

Plataforma multilateral. Cuando tenemos dos o más segmentos de clientes y uno depende del otro, lo llamamos plataforma multilateral o mercado multilateral.

### **Segmento de Mercado - Restaurantes**

Vimos un tipo de mercado en el que todas las personas podían utilizar el servicio, porque el objetivo era difundir la música. Si abrimos un restaurante, todas las personas que necesiten comprar algo para comer podrían ser nuestro público. ¿Cómo se llama este tipo de segmento de mercado?

Rta.

Mercado de masa.

Cuando hablamos de mercado de masas, estamos pensando en un tipo de mercado en el que varios segmentos de clientes, con características socioeconómicas y hábitos de consumo totalmente diferentes, pueden tener intereses comunes, y este interés común, puede ser por ejemplo escuchar música. En un restaurante, el interés común es comer.

En el caso de Spotify, por ejemplo, puedes encontrar desde Reggaeton hasta Heavy Metal, pasando por Gospel y música country. Cualquier persona que esté interesada en la música, puede darse de alta en Spotify y disfrutar del estilo que más le convenga. En el caso de que Spotify se centrara, por ejemplo, sólo en la música gospel, sólo las personas que escuchan este estilo serían el segmento de clientes principal.

### **Segmento de Mercado - Tinder**

En nuestros vídeos sobre segmentos de clientes, citamos el ejemplo de una aplicación de citas: Tinder. Esta aplicación se centra en las personas que buscan una relación. Cuando tenemos un negocio en el que los clientes son una pequeña porción de personas, ¿cómo se llama este tipo de segmento de clientes?

Rta.

Mercado de Nicho.

Cuando hablamos de nicho de mercado, o de segmentos de clientes nicho, estamos refiriéndonos a clientes con necesidades específicas que hasta entonces habían sido poco exploradas.

Antes de Tinder, existían otras Apps que basaron su modelo de negocio para este tipo de clientes, sin embargo, la forma de interacción de Tinder fue tan exitosa que ese mercado de clientes encontró valor en la app y la usa con más frecuencia que otras opciones disponibles en el mercado.

### **Segmento de Mercado - Bancos**

Hay un segmento de clientes que mencionamos en videos anteriores citando como ejemplo el mercado de tarjetas de crédito y los bancos, cuyos clientes tienen diferente poder adquisitivo y propuestas de valor, que se ven impactados por diferentes canales, pero todos tienen necesidades similares en el servicio contratado. Cuando tenemos un segmento de clientes con estas necesidades, ¿cómo lo llamamos?

Rta.

Mercado segmentado.

Cuando tenemos un mercado segmentado, tenemos clientes con diferentes intereses y características que son atendidos por nuestro negocio, pero cada uno tendrá diferentes características en las demás secciones del Business Model Canvas.

### **Lo que aprendimos: Como va nuestro Canvas**

Luego de completar el segmento de clientes para la App "Me Transporte", dejaremos en el siguiente link la plantilla del Business Model Canvas que hemos completado en el módulo 2. En este curso vamos a llenar el Business Model Canvas.

_Ver : Segmento_de_Clientes_Me-Transporte.pdf_

---

## **Propuesta de valor**

### **Acerca de la propuesta de valor**

Según el libro de Business Model Generation, después de identificar el segmento de clientes de nuestro negocio, el siguiente paso es entender la propuesta de valor para ese segmento de clientes. Así, es correcto afirmar que:

Rta.

La propuesta de valor crea valor para un segmento de clientes con una combinación de elementos direccionados específicamente para las necesidades de ese segmento.

La propuesta de valor es la sección que identifica aquello que hace que un cliente decida entre una y otra empresa. Muestra las necesidades y los problemas que resolvemos específicamente para ese segmento de clientes y lo que esperan de nuestro producto o servicio. Esto puede mostrarnos el éxito o fracaso de lo que estamos ofreciendo.

### **Tipos de propuesta de valor**

En nuestro video vimos que existen dos tipos de propuesta de valor. ¿Cuáles son?

Rta.

Cualitativos y Cuantitativos. Cuando hablamos de propuesta de valor, tenemos que considerar que existen dos tipos: Cuantitativos (que pueden contarse con números) y cualitativos (que son relativos a la calidad). Estos deben ser considerados y percibidos por nuestro segmento de clientes. Por ejemplo, un precio menor puede ser una propuesta de valor cuantitativa, mientras que el diseño del producto, el color y el material resistente puede ser cualitativo.

### **Caso Snapchat**

Cuando estamos abriendo un negocio con un producto o servicio novedoso que nadie ha visto hasta entonces, (como vimos en el caso de Snapchat), ¿cuál es el tipo de propuesta de valor que estamos entregando?

Rta.

Novedad. Las propuestas de valor de este tipo incluyen algo que es completamente nuevo y nunca visto hasta ahora. Los clientes no pensaban en esa necesidad, ya que no existe algún producto o servicio de ese tipo.

### **Caso Rolex y Ferrari**

En el video anterior, vimos el caso de marcas como Rolex y Ferrari que producen productos deseados y con un precio elevado. También vimos el caso de Vans, dirigida también a un público específico. ¿Cuál es el tipo de propuesta de valor que identificamos con esas marcas?

Rta.

Marca / Estatus. Cuando hablamos de una propuesta de valor del tipo marca / estatus, tenemos los casos de Vans, Rolex y Ferrari. Otro ejemplo, podría ser MAC, la marca de maquillaje usada por algunas famosas. Por más de que no todos tengan dinero para comprar ese tipo de maquillaje, se esfuerzan para adquirirlo y estar a la "moda".

### **Compra de un vehículo usado**

En el video anterior, mencionamos el escenario al comprar un carro usado y la compra de un carro nuevo. Comentamos que en algunos casos, el concesionario ofrecía un seguro de reparaciones por años o kilómetros recorridos. Este tipo de características pueden influir en la decisión de compra. ¿Cuál es el tipo de propuesta de valor que ellos entregan con este tipo de seguro?

Rta.

Reducción de riesgo. La reducción de un riesgo puede ser una propuesta de valor que ayuda a justificar el precio para los clientes. También tenemos en esta categoría la garantía antifraude que ofrece una plataforma de pago para las tiendas que cubre el valor de la compra en caso que sufran un ataque de fraude o también, la garantía extendida de un ventilador que es superior a la garantía por daños de la competencia.

### **Caso del iPod y el iPad**

Apple es una empresa que busca innovar en sus productos. Pensando en eso, mencionamos el caso del iPod que integraba iTunes y fue una innovación en su momento de la industria musical. También vimos el caso del iPad, donde permitía tener una pantalla mayor sin necesidad de tener un computador portátil ¿Cuál es el tipo de propuesta de valor que Apple entrega con el iPod y el iPad?

Rta.

Conveniencia y Usabilidad. Apple y aquellas empresas que buscan facilitar el uso de un producto o un servicio tienen la propuesta de valor de conveniencia y usabilidad. Los clientes usarán sus productos porque son más simples de usar que otros.

### **Caso Netflix**

Mencionamos también en el video anterior el caso de Netflix que ofrece la posibilidad de ver contenido cinematográfico a través de internet pagando el costo de una suscripción. Comparamos ese caso con las salas de cine y conversamos acerca de la propuesta de valor que ofrece. Según lo visto en la clase anterior, ¿cuál es la propuesta de valor relacionada con este caso de negocio?

Rta.

Accesibilidad. Empresas como Netflix entregan una propuesta de valor llamada accesibilidad. Esto quiere decir que, buscan entregar un producto o servicio que no estaba disponible. En el caso de Netflix, es dar acceso y popularizar el contenido cinematográfico.

### **Computadores para Gamers**

Si fuéramos a abrir una empresa de computadores qué consideramos "potentes" y nuestros clientes principales son Gamers que buscan potencia y un buen rendimiento en los juegos para computador. Si queremos hacer énfasis en ese discurso y queremos entregar ese valor para ese segmento de clientes ¿cuál sería nuestro tipo de propuesta de valor?

Rta.

Desempeño. Efectivamente, el desempeño es la propuesta de valor principal de nuestro negocio de computadores para Gamers. Sabemos que nuestro segmento de clientes busca el mejor rendimiento en sus juegos y no abre la posibilidad de bloqueo y bajo rendimiento gráfico.

### **Caso PayPal**

PayPal normalmente es usado para transferir dinero entre compradores y tiendas. Si no se finaliza una transferencia entre dos personas, no entregaría la propuesta de valor que promete.Pensando en todo eso ¿cuál es la propuesta de valor que debe ser ofrecida por PayPal?

Rta.

El trabajo, hecho. Este tipo de empresas que entregan la propuesta de valor "el trabajo, hecho" tienen como foco el resultado, es decir, su objetivo principal es permitir que los clientes cumplan determinada tarifa con éxito, o sea, el funcionamiento de la empresa respecto al producto y/o servicio debe ser perfecto.

### **Caso Uber**

En el ejemplo de Uber que vimos en clase encontramos dos tipos de propuesta de valor. Una de ellas, permitía la posibilidad de que los usuarios escojan cuál es el tipo de carro que desean basado en el precio y las comodidades que esa tarifa contempla. La segunda, permitía tener una ventaja competitiva para los pasajeros al poder comparar el servicio de Uber con los taxis.

Según el enunciado, ¿cuáles son las dos propuestas de valor que ofrece Uber?

Rta.

Personalización y Reducción de Costo. Cuando hablamos de la propuesta de valor "personalización", estamos refiriéndonos a productos y servicios que se adecúan conforme a la necesidad de los clientes. Uber ofrece varias opciones de vehículos disponibles para sus usuarios: Entre ellos, Uber X, Uber Pool, Uber Black. Por otro lado, dentro de los valores cuantitativos, reducción de costo puede ser un gran valor para entregar a los clientes e incluso, podría ser un factor determinante entre la elección de un mismo producto ofrecido por empresas diferentes.

### **Lo que aprendimos: Cómo va nuestro Canvas - Propuesta de valor**

Luego de completar el módulo de la propuesta de valor para la App "Me Transporte", dejaremos en el siguiente link la plantilla del Business Model Canvas que hemos completado hasta ahora. Ya tenemos dos secciones completas del Business Model Canvas.

_Ver : Propuesta_de_valor_Me-Transporte.pdf_

Descarga el archivo y continuaremos con las demás secciones en los módulos siguientes.

¡Continuamos!

---

## **Canales**

### **Canales**

Según lo que vimos en el video anterior, ¿qué son los canales?

Rta.

Amplía el conocimiento del segmento de clientes para que ellos puedan ver y corroborar la propuesta de valor que ofrece mi negocio a través de la comunicación que hacemos hacia ellos. De forma concreta, el canal es como vamos a comunicarnos con nuestros clientes y de qué forma entramos en contacto con ellos.

### **Caso Facebook**

En el video anterior, hablamos de Facebook y Google. En estos canales podemos hacer publicidad de nuestro negocio y llegar fácilmente a una audiencia. Según lo que vimos, ¿a qué tipo de canal corresponde y qué significa?

Rta.

Canal de socios indirectos. Este tipo de canal no pertenece a la empresa, sólo realiza una colaboración, ya sea monetaria o no, para promocionar su producto y/o servicio con el fin de dirigir a su consumidor a comprar el producto o contactar a través de un canal propio directo.

Los canales de socios comerciales son indirectos y podemos tener un costo de uso mucho menor que si tuviéramos nuestro propio canal, sin embargo, el margen de ganancia también será menor porque tenemos que pagar primero a este canal y luego obtener un beneficio económico.Por ejemplo, cuando hacemos uso de Facebook, tenemos que pagar para publicar un anuncio en su plataforma y esperar que, al ver la comunicación, los usuarios adquieran nuestro producto y/o servicio. Posterior a la venta o adhesión al servicio, podemos pasar al canal propio directo.

### **Varios tipos de Canales**

Cuando definimos cuáles eran nuestros segmentos de clientes y la propuesta de valor para cada uno de ellos, necesitábamos identificar cómo nos íbamos a comunicar con ellos. ¿Cuáles son los tipos de canales que citamos en el video anterior?

Rta.

Canal propio directo e indirecto y canal de socios comerciales. Dentro de los canales, tenemos los canales propios y los canales de socios comerciales. Los canales propios pueden ser directos e indirectos. Los canales de socios comerciales siempre son indirectos.

### **Canales propios directos**

Cuando hablamos de canales propios directos, ¿a qué nos referíamos?

Rta.

Son los canales que son de nuestro negocio y el cliente tiene contacto directo con nosotros, diferente de una comunicación por televisión o radio, ya que no somos dueños de un canal de radio o televisión. Dentro de los canales, tenemos los canales propios y los canales de socios comerciales. Los canales propios pueden ser directos e indirectos. Los canales de socios comerciales siempre son indirectos.

### **Lo que aprendimos: Resumen de los Canales**

Finalizamos el módulo 4 y durante esta clase, vimos aquellos canales que permiten entrar en contacto con nuestro segmento de clientes y brindar la propuesta de valor.

Ya tenemos tres opciones completas en el diagrama del Business Model Canvas.

_Ver : Canales_Me-Transporte.pdf_

---

## **Relación con los clientes**

### **Sobre la relación con los clientes**

Hay un sector en el Business Model Canvas llamado Relación con el Cliente. Cuando estamos completando esta sección, queremos conocer lo siguiente:

Rta.

Identificar y describir cómo voy a hablar con cada segmento de clientes. En la sección de Relación con el Cliente, sabemos cómo será nuestra relación con cada segmento de clientes que tiene nuestro negocio o empresa.

### **Relación con los clientes - Me transporte**

Comentamos el caso de la App Me Transporte. En nuestro Brief, habíamos visto un servicio automático que responde a los usuarios a través de la aplicación. ¿Cómo se llama este tipo de relación con el cliente?

Rta.

Servicio de atención automático. Un servicio automático busca reducir los costos a largo plazo y evita la contratación de personas, sin embargo, puede ser muy costoso de desarrollar. Hay servicios automáticos, que son capaces de dialogar con los clientes, recomendar recetas, libros e incluso resolver preguntas complejas solo con conversar con el servicio virtual.

### **Relación con los clientes - Nubank**

Nubank es reconocido por su servicio de atención al cliente en el que alguien atiende las dudas, problemas o preguntas que los clientes tienen con sus productos. Cuando hablamos de un servicio de atención al cliente como el de Nubank, ¿de qué tipo de relación con el cliente estamos hablando?

Rta.

Servicio o asistencia personal. El servicio o atención personal se basa en la interacción humana, en acercar la empresa a sus clientes, ya sea antes de una venta o después de la misma. La intención es acercar la empresa al cliente y mostrarle que siempre tendrá el soporte que necesite.

### **Relación con los clientes - Adobe**

Una empresa como Adobe tiene un producto orientado al mercado corporativo. En esos casos, un ejecutivo comercial va a la oficina de una empresa para ofrecer licencias masivas. ¿Cómo llamamos a este tipo de relación con el cliente?

Rta.

Servicio o asistencia personal dedicada. Servicio o asistencia personal dedicada, tiene uno o más representantes exclusivos para una sola empresa. Esto ayuda no sólo a mantener a los grandes clientes de una empresa, sino también a mostrar el valor del producto o servicio, lo que permite una relación más estrecha, profunda y a largo plazo con los clientes.

### **Relación con los clientes - Youtube y TikTok**

Tanto Youtube como TikTok dependen de los usuarios que alimentan la plataforma con contenidos para que otros usuarios accedan a ellos. Cuando se tiene una relación con el cliente como la de Youtube y TikTok ¿cómo la llamamos en el video anterior?

Rta.

Creación colectiva. En la creación colectiva, la empresa busca acercarse cada vez más a los usuarios que hacen que su negocio prospere y funcione. En el caso de Youtube y TikTok, se estimula a aquellos usuarios que crean contenido a través de premios e incentivos para tener contenido actualizado, que se traduce en más visitas dentro de la plataforma.

### **Relación con los clientes - Alura**

En la página de Alura tenemos un lugar de preguntas y respuestas, donde los estudiantes y los instructores pueden responder a las preguntas de otros estudiantes. ¿Cómo se llama este tipo de relación con el cliente?

Rta.

Comunidades. Las comunidades ayudan a los usuarios a responder preguntas entre sí y facilitan las conexiones entre los clientes que participan en esa comunidad. También permite conocer mejor a los segmentos de clientes y evidencia los problemas que pueden tener, lo que buscan, lo que suelen preguntar y centrarse en la resolución de esos problemas. Esto podría suceder mediante la creación de un nuevo segmento de clientes o cambiando el mismo modelo de negocio del que los clientes se quejan.

### **Relación con los clientes - Facebook**

Las empresas que tienen páginas en Facebook y quieren hacer anuncios, pueden tener preguntas durante este proceso. Por eso, Facebook ha desarrollado una página exclusiva en la que los usuarios pueden resolver las preguntas frecuentes y consultar otras preguntas que otros usuarios ya han respondido y formulado. Todo ello con unos pocos clics del usuario en un solo lugar. ¿Cuál es ese tipo de relación con los clientes?

Rta.

Autoservicio. El autoservicio proporciona a los usuarios todos los medios para resolver sus problemas o preguntas sobre el servicio y/o el producto.

### **Tipos de relación con los clientes**

Hay varios tipos de relaciones con los clientes. ¿Cuáles son los tipos que vimos hasta ahora en clase?

Rta.

Servicio automático, asistencia personal, asistencia personal dedicada, creación colectiva, comunidades y autoservicio. Cuando hablamos de la relación con los clientes identificamos como nuestro negocio va a comunicarse con ellos. Dentro de las relaciones con los clientes, enmarcamos las siguientes: El servicio automatizado, la asistencia personal, la asistencia personal dedicada, la creación colectiva, las comunidades y el autoservicio. Cada uno de estos tipos puede afectar directamente la experiencia del segmento de clientes con nuestro producto y/o servicio.

### **Lo que aprendimos: Resumen de la relación con los clientes**

En este módulo, completamos la parte de "relación con los clientes" para la App Me Transporte.

_Ver : Relacion_con_los_clientes_Me-Transporte.pdf_
