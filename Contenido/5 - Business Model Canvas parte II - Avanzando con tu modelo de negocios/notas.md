# **Business Model Canvas parte II - Avanzando con tu modelo de negocios**

## **Fuentes de Ingresos**

### **Fuente de Ingresos**

En el video anterior, vimos que en la sección de Fuente de Ingresos del Business Model Canvas, empezamos a hablar de cómo ganar dinero. ¿Cuál es la importancia de este sector?

Rta.

La fuente de ingresos es la sección del Canvas en el que vemos cómo generar dinero a través de cada segmento de clientes. En la sección de la fuente de ingresos, conseguimos identificar dónde vamos a ganar dinero a través de cada segmento de clientes. A partir de este punto le restamos los costos y obtenemos el beneficio.

### **Caso: Disney+ y Deezer**

Marcas como Disney+ y Deezer, cobran una cuota para que el cliente pueda acceder a la plataforma y paga ese mismo valor cada mes para poder utilizar el servicio. ¿Cómo llamamos al tipo de fuente de ingresos con estas características?

Rta.

Cuota por suscripción. Al realizar el pago, el cliente que utiliza servicios como Disney+ y Deezer, tiene acceso a todas las funcionalidades del servicio y/o producto. A este tipo de fuente de ingresos que tienen estas empresas, lo llamamos cuota de suscripción.

### **Caso: Operadores de telefonía móvil**

En nuestra clase, mencionamos los planes prepago y pos-pago que ofrecen los operadores de telefonía móvil. Vimos que estas empresas cobran una cuota extra cuando los usuarios superan la cantidad contratada en el plan o paquete de servicios. ¿Cómo se llama este tipo de fuente de ingresos?

Rta.

Tasa por uso. A diferencia de la cuota de suscripción, el usuario paga cuanto utiliza el servicio cuando la fuente de ingresos es una tasa por uso. Es decir, cuanto más se usa el servicio, paga más.

### **Caso: Fabricante de vehículos**

Cuando hablamos de fabricantes de automóviles como Fiat, Nissan, Chevrolet, Volkswagen, entre otros, esperamos que ganen dinero, es decir, que su fuente de ingresos provenga de esta venta de vehículos. ¿Cómo se llama este tipo de fuente de ingresos?

Rta.

Venta de activos. Cuando hablamos de la venta de activos, estamos haciendo referencia a la fuente de ingresos basado en la venta de un producto a partir de un intercambio de dinero.

### **Caso: Microsoft Office**

En clase mencionamos que para acceder a los programas Office de Microsoft, debemos pagar un valor para poder instalarlo en el computador. Si intentamos instalarlo en otro computador con el mismo número de serie que recibimos de Microsoft, tendremos un error en la instalación, ya que incumplimos con los acuerdos de la empresa para usar ese software.

Para el tipo de fuente de ingresos de Microsoft, con sus programas de Office, ¿cuál es el nombre que utilizamos en el Business Model Canvas?

Rta.

Licencias. Cuando hablamos de fuentes de ingresos del tipo “licencias”, estamos justamente refiriéndonos a las empresas que han creado algún servicio y permite el uso de este a otra empresa o a un usuario común, a cambio de un valor económico. Cuando no se paga o se incumplen los acuerdos iniciales, el "comprador de esa licencia" ya no puede utilizarla.

### **Caso: Inmobiliarias**

Algunos emprendedores que están abriendo sus startups, están interesados en tener una oficina, ya sea en un coworking o incluso alquilando un local para uso comercial de su propia empresa.

Para ello, suelen entrar en contacto con inmobiliarias que trabajan exclusivamente con oficinas. ¿Cómo se llama el tipo de fuente de ingresos de estas agencias inmobiliarias?

Rta.

Préstamos y Arriendos. Cuando una empresa adopta la fuente de ingresos de Préstamos y Alquileres, estamos afirmando que la empresa permitirá a un tercero utilizar su producto y/o servicio mediante un pago periódico y durante un periodo determinado.

### **Caso: YouTube**

YouTube recibe dinero a través de un pago por la publicidad de otras empresas dentro de su plataforma. ¿Cómo llamamos a este tipo de fuente de ingresos, según la clasificación de Generación de Modelos de Negocio?

Rta.

Anuncios. Plataformas como YouTube, algunos eventos y festivales, emisoras de radio y televisión, etc. tienen sus fuentes de ingresos enfocadas el contenido y proporcionan espacio para los medios de comunicación, ganando dinero a través de los anunciantes que utilizan este espacio.

### **Caso: MercadoPago**

Cuando citamos a MercadoPago y otras plataformas de pago, vimos que ganan dinero en la intermediación de las compras entre comerciantes y los compradores, a través de una comisión. ¿Cómo se llama el tipo de fuente de ingresos de MercadoPago?

Rta.

Tasa o Gasto de Corretaje. Cuando pensamos en plataformas de pago, bancos, entre otros, que tienen su fuente de ingresos del tipo “Tasa o gasto de corretaje”, hablamos de empresas que intermedian entre dos o más partes y que, por tanto, reciben un porcentaje sobre cada una de las transacciones realizadas.

### **Lo que aprendimos**

Finalizamos el módulo 1 del curso Business Model Canvas, Parte 2. Durante esta clase, vimos las formas de generar ingresos, vimos algunos ejemplos y encontramos aquella que se acomoda a nuestra aplicación Me Transporte.

Nos preguntamos ¿por qué valor está dispuesto a pagar cada segmento de mercado? y vimos que podemos tener una o varias fuentes de ingresos en cada segmento de mercado.

Así va nuestro Business Model Canvas:

_Ver : Fuente_de_Ingresos_Me-Transporte.pdf_

---

## **Recursos Clave**

### **Recursos Clave**

Al completar la sección de Recursos Clave del Business Model Canvas, debemos entender cuál es el significado de este elemento. Según lo anterior ¿cuál es la importancia de esta zona del Canvas?

Rta.

Cuando pensamos en los recursos clave, estamos enumerando los recursos más importantes que necesitamos tener, para que nuestra empresa funcione.

Los recursos básicos son los responsables del funcionamiento del negocio y están directamente relacionados con que una empresa consiga crear y ofrecer una propuesta de valor con sus segmentos de clientes, así como permitir que la relación con estos clientes funcione de la mejor manera posible.

### **Caso: Abriendo nuestra propia empresa**

A la hora de poner en marcha un negocio, debemos tener en cuenta cuáles son los principales recursos para que funcione bien. Entre los recursos, tenemos un equipo de profesionales que vamos a contratar. Cuando hablamos de personas, ¿cómo se llama este tipo de recurso principal?

Rta.

Recurso Humano. Cuando hablamos de recursos clave, hay que pensar en el recurso humano. Este tipo de recurso siempre está presente en las empresas, incluso, por ejemplo, si el servicio que se ofrece es completamente automatizado, siempre habrá alguien que ha creado este servicio. Incluso, siempre habrá alguien dando soporte a ese servicio en un equipo técnico. Es casi imposible que un negocio no necesite de un recurso humano.

### **Caso: Alura y Amazon**

En clase mencionamos una empresa como Alura y Amazon, que necesitan un lugar tanto para grabar los cursos como para almacenar los productos que venden. Cuando vimos este tipo de recurso clave, ¿cómo lo llamamos?

Rta.

Recurso Físico. Cuando hablamos de los recursos principales de una empresa, no sólo hablamos del lugar físico en el cual está localizado el negocio. También hacemos referencia a todo lo que posee, desde computadores, sillas, escritorios, puntos de distribución y logística. Todos estos bienes se incluyen en el recurso físico de una empresa.

### **Caso: Una editorial**

Las editoriales necesitan autores calificados que deseen publicar sus libros, y así, conseguir venderlos. Cuando hablamos de estos autores que son imprescindibles para que una editorial funcione, ¿cómo se llama este recurso clave?

Rta.

Recurso Intelectual. Las editoriales son empresas cuyo recurso intelectual es un factor determinante para generar ingresos. Sin autores, la editorial no tendrá libros que vender. Por lo tanto, cuando hablamos del recurso clave intelectual, nos referimos a las propiedades de conocimiento que debemos tener para que la empresa pueda funcionar y generar valor.

### **Un recurso indispensable**

Las empresas que están comenzando no siempre pueden contratar los recursos intelectuales, humanos y físicos, porque carecen de otro tipo de recursos clave. ¿Cómo se llama este tipo de recurso?

Rta.

Recurso Económico. Cuando hablamos de recursos clave económicos, nos referimos a aquellos recursos financieros que ayudan a potenciar el negocio o un área específica del mismo.

### **Tipos de Recursos Clave**

Ahora, para resumir todo lo que hemos visto, ¿cuáles son los tipos de recursos clave que hemos aprendido en este módulo?

Rta.

Recursos económicos, intelectuales, humanos y físicos.

### **Lo que aprendimos**

Terminamos el segundo módulo del curso y así está quedando nuestro Canvas.

_Ver : Recursos_Me-Transporte.pdf_

En este módulo vimos la importancia de los recursos clave, e incluso, profundizamos en los cuatro tipos de recursos que necesita un negocio: Económicos, intelectuales, humanos y físicos.

También vimos que en el caso de la aplicación Me Transporte, necesitamos los cuatro recursos, comenzando por el económico, luego contratar un recurso humano que nos permita tener esa atención personal para nuestros clientes, un lugar físico y finalmente, recurso intelectual.

---

## **Actividades Clave**

### **Actividades Clave**

Cuando abrimos una empresa, por ejemplo, una empresa de desarrollo de software, tenemos que desarrollar una serie de actividades para entregar el software a los clientes. Esto justamente se llama “actividades clave”. Teniendo esto en cuenta, ¿cuál es el significado de las actividades clave?

Rta.

Las actividades clave son las acciones más importantes que una empresa debe realizar para lograr su correcto funcionamiento. En las actividades clave verificamos cuáles son las actividades y procesos más importantes por realizar para que la empresa tenga éxito en su funcionamiento.

### **Caso: Fabricantes de celulares**

Cuando hablamos de las actividades clave de fabricantes de celulares como Motorola y Samsung, sabemos que primero tienen que diseñar, construir y vender esos teléfonos móviles. Con esto, genera ingresos para el negocio.

¿Cómo llamamos este tipo de actividades clave?

Rta.

Producción. Cuando una empresa tiene como actividad clave “producción”, hacemos referencia al diseño y construcción para crear un producto final. La empresa entrega algo físico a los segmentos de clientes y esta actividad clave es muy común en los negocios industriales.

### **Caso: Consultoría en Experiencia de Usuario (UX)**

En nuestra clase, citamos a las empresas de consultoría de diseño de experiencia de usuario (UX) y mencionamos que, en general, son contratadas para resolver y encontrar los problemas de los clientes de otras empresas, y que estas empresas no podrían identificar y resolver por sí mismas.

Estas empresas de consultoría tienen una actividad clave llamada:

Rta.

Resolución de Problemas. Las empresas que tienen este tipo de actividad clave se preocupan de que su personal esté constantemente informado y, por tanto, sea capaz de resolver los problemas de los clientes de sus clientes.

### **Caso: Aplicación Me Transporte**

En el caso de Me Transporte, (la aplicación que hemos estudiado a lo largo del curso), los conductores y pasajeros tienen que entrar en la aplicación para resolver cierto objetivo. Este objetivo puede ser, ya sea aceptar el viaje de un pasajero o solicitar un viaje con un conductor.

Por eso, es necesario que la aplicación funcione sin errores. En el caso de Me Transporte, tenemos una actividad clave del tipo:

Rta.

Plataforma / Red. Cuando mencionamos a las empresas cuya actividad clave es plataforma / red, estamos hablando de empresas que necesitan que sus herramientas funcionen bien para que su negocio siga operando. Estas empresas deben mantener y mejorar su software continuamente.

### **Tipos de Actividades Clave**

Durante este módulo, hablamos de tres tipos de actividades clave. ¿Cuáles fueron esos tipos que citamos en los videos anteriores?

Rta.

Producción, resolución de problemas y plataforma / red. Las actividades clave en las que se puede encajar un modelo de negocio, son producción, resolución de problemas y plataforma / red.

### **Lo que aprendimos**

Finalizamos el módulo 3 del curso Business Model Canvas. Durante esta clase, estudiamos las actividades clave, como aquellas acciones más importantes que una empresa debe realizar para tener éxito en su modelo de negocio.

También vimos tres tipos de actividades clave y encajamos a nuestra app, Me Transporte en uno de esos tipos. Así está quedando nuestro Canvas:

_Ver : Actividades_Clave_Me-Transporte.pdf_

En el caso de la App Me Transporte, tendremos una actividad clave del tipo "Plataforma / Red". Esto nos obligará a que nuestra aplicación esté funcionando con todas las características que ofrece para conductores, pasajeros, incluso, las bicicletas.

---

## **Asociaciones Clave**

### **Asociaciones Clave**

Para encontrar las asociaciones o socios clave, debemos saber a qué se está haciendo referencia dentro del Business Model Canvas. Teniendo en cuenta este enunciado, podemos afirmar respecto a las asociaciones clave que:

Rta.

Las asociaciones clave permiten describir nuestra red de proveedores y socios que necesitamos para que todo funcione, optimizando el modelo de negocio y reduciendo el riesgo. Pensando en las asociaciones clave, nos referimos a los proveedores y socios, que son esenciales para que nuestro modelo de negocio funcione.

### **Caso: Aplicaciones de movilidad urbana**

Los conductores de Uber, Didi y Beat son fundamentales en la operación de estos negocios. Según lo que vimos en el video anterior, ¿cómo se llama ese tipo de asociación clave?

Rta.

Recursos o Actividades Particulares. Cuando hablamos de Recursos o Actividades Particulares, nos referimos a aquellos recursos que no tenemos y que necesitamos de un socio para poder poner en marcha nuestro negocio y poder cumplir con todas nuestras actividades clave.

### **Caso: Fabricantes de bicicletas**

El problema de las bicicletas de la App Me Transporte estaba siendo perjudicial para el negocio porque no estaba permitiendo que todas las funcionalidades publicadas en la aplicación estuvieran disponibles. En el anterior video, citamos un tipo específico de asociación clave que podría ayudar, incluyendo fabricantes de bicicletas como Trek. ¿Cuál es el nombre de este tipo de asociación principal?

Rta.

Optimización y Economía de Escala. Las empresas que tienen como principal asociación del tipo “optimización y la economía de escala”, se centran en tener una reducción de sus costos operativos al asociarse con otras empresas.

### **Caso: Bike Itaú y Muvo**

Como mencionamos en clase, podemos tener otro tipo de asociación principal que podría resolver el problema de las bicicletas en nuestra App. Podríamos tener una alianza con Bike Itaú o Muvo, que ya tienen el servicio de bicicletas. ¿Cómo se llama este tipo de asociación principal?

Rta.

Reducción de Riesgos e Incertidumbre. Cuando nos referimos a la asociación del tipo “reducción de riesgo e incertidumbre”, hablamos justamente en alianzas con otras empresas que ya trabajan con aquel producto o servicio que queremos. Incluso, podemos competir con aquellas empresas más conocidas en el mercado o hacer una para llegar a más clientes.

### **Tipos de Asociaciones Clave**

¿Cuáles son los tipos de asociaciones clave que hemos citado en clase y que han sido nombrados por los autores de Business Model Generation y utilizados en el Business Model Canvas?

Rta.

Reducción de riesgos e incertidumbre, optimización y economía de escala, recursos y actividades particulares. Cuando hablamos de asociaciones clave, es decir, las asociaciones con proveedores y socios que son esenciales para el funcionamiento de nuestro negocio, tenemos los siguientes tipos: reducción de riesgos e incertidumbres, optimización y economía de escala y actividades particulares.

### **Lo que aprendimos**

Finalizamos el módulo 4 del curso Business Model Canvas. Durante este módulo, estudiamos las asociaciones clave, identificando a los proveedores y socios, que son esenciales para que nuestro modelo de negocio funcione.

Vimos la importancia de las asociaciones clave, e incluso, profundizamos en los tres tipos de recursos que puede tener un negocio: Reducción de riesgos e incertidumbre, optimización y economía de escala, recursos y actividades particulares.

Así va nuestro canvas:

_Ver : Asociaciones_Clave_Me-Transporte.pdf_

---

## **Estructura de Costos**

### **Estructura de Costos**

El último componente que veremos en nuestro curso, es la sección de estructura de costos. ¿Qué significa esta sección y qué contiene?

Rta.

En la estructura de costos comprobamos y describimos los costos que implica el funcionamiento de un modelo de negocio, la creación y la entrega de valor a mis clientes. En la estructura de costos entendemos y describimos los costes necesarios para el funcionamiento del negocio en el que estamos trabajando.

### **Caso: Conductores de Didi**

Cuando conversamos del tipo de estructura de costos de los conductores que trabajan en aplicaciones como Didi, Uber, Beat, incluso, en la App Me Transporte, ¿a qué tipo de estructura de costos nos estábamos refiriendo?

Rta.

Estructura de costos variable. Los costos variables reciben este nombre porque sus valores no son siempre los mismos, variando justamente en función del volumen.

### **Caso: Los salarios**

Cuando tenemos empleados que ganan el mismo sueldo todos los meses sin incluir algún bono adicional a sus salarios, ¿cómo se llama este tipo de estructura de costos?

Rta.

Estructura de costos fijos. Los costos fijos son siempre los mismos, independientemente de la cantidad de trabajo o producto producido, tales como la nómina de una empresa.

### **Caso: Carro compartido**

Vimos el caso de UberPool, el servicio de carro compartido de Uber, donde se optimiza la ruta para compartir el mismo vehículo con más pasajeros, y así, por el mismo trayecto, los conductores ganan más dinero mientras los pasajeros pagan menos. ¿Qué tipo de estructura de costos tenemos en este caso?

Rta.

Estructura direccionada según costos. UberPool es un buen ejemplo de una estructura direccionada según costos. Las empresas que tienen esta iniciativa pretenden minimizar el costo siempre que sea posible, tanto para la empresa como para los clientes.

### **Caso: Hoteles de Lujo**

En clase vimos el caso de los hoteles de lujo, que ofrecen determinadas propuestas de valor que justifican sus costos. ¿Cómo se llama este tipo de estructura de costos?

Rta.

Estructura direccionada por el valor. Las empresas con estructuras de costos direccionadas por el valor no se preocupan por el valor financiero del servicio, ni para la empresa ni para el cliente. Lo que realmente les preocupa es la propuesta de valor que van a ofrecer y con ella justifican el precio del servicio y/o del producto.

### **Caso: Cuponatic**

En nuestros vídeos mencionamos sitios como Cuponatic, centrados en las compras colectivas, que pueden disminuir el costo para el usuario según la demanda. ¿Cómo se denomina este tipo de estructura de costos?

Rta.

Estructura Economía de Escala. Cuando nos referimos a empresas como Cuponatic con estructura de costos del tipo “economía de escala”, estamos ante una empresa que obtiene más beneficios a medida que aumenta la producción del producto y/o servicio.

### **Caso: Disminuyendo el equipo**

Cuando una empresa reduce su equipo interno y sigue atendiendo el mismo volumen, pero reasignado dentro de ese equipo reducido los clientes existentes, tenemos un tipo de estructura de costos:

Rta.

Estructura de costos de economías de campo. Las empresas que tienen el tipo de estructura de costos de economía de campo, se centran en tener una operación que satisface una demanda mucho mayor, repartiendo los recursos en varios segmentos de esa demanda.

### **Tipos de estructuras de Costos**

A lo largo de este módulo hemos visto los diferentes tipos de estructuras de costos que puede tener un negocio. ¿Cuáles son esos tipos que vimos durante esta aula?

Rta.

Los tipos de costos que mencionamos en el aula y están en el libro del Business Model Generation son: Fijos, variables, estructura direccionada por el valor, según costos, economía de escala y de economías de campo.

### **Lo que aprendimos**

Terminamos el módulo 5 del curso Business Model Canvas y ya tenemos completo nuestro lienzo usando como caso práctico la App Me Transporte.

Así quedó nuestro Canvas:

_Ver : Estructura_de_Costos_Me-Transporte.pdf_

Durante esta aula vimos todas las estructuras de costos que puede tener un modelo de negocio según el libro del Business Model Generation. Adicionalmente, encontramos varios ejemplos que ayudarán a identificar el tipo de estructura de costos de tu negocio.

A continuación, vamos a resumir lo que vimos durante el curso y adicionalmente, estudiaremos dos tipos de Canvas, aparte del que vimos en este curso, que podrán ayudarte a materializar tu idea de negocio y serán útiles en otros casos.

¡Muy bien! Ya tienes las herramientas para completar el Canvas de tu propia idea de negocio.

---

## **Proximos pasos**

### **Lo que aprendimos**

En el último video vimos dos tipos de Canvas que te ayudarán en otros tipos de ideas de negocio. Cuando necesites evaluar cualquier hipótesis de tu idea de negocio, puedes usar el Lean Model Canvas.

Te dejamos el link para que uses tu propia plantilla.

_Ver : Lean_Model_Canvas.jpg_

Puedes guardarla en tu Google Drive para futuras consultas.

También te mostramos el Social Business Model Canvas, orientado a proyectos con enfoque social que no necesariamente tienen la intención de ganar dinero. Para esto, el Social Business Model Canvas incluye el tipo de intervención y una sección para reinvertir esos recursos en el bienestar de aquellos beneficiarios.

_Ver : Social_Business_Model_Canvas.jpg_

De igual forma, puedes guardarlo en tu propio Google Drive para comenzar a editarlo.

¡Espero que este curso haya sido muy útil para ti! Ya tienes las herramientas para estructurar tu modelo de negocio y poder mostrárselo a un inversionista. El Business Model Canvas no acaba aquí, puedes iterar las veces que quieras. Cuestiónalo, revísalo y vuelve al Canvas cada vez que sea necesario.

¡Realmente es una poderosa herramienta para aterrizar tus ideas de negocio!
