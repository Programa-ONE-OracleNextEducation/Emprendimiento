# **Lean Startup - Método eficaz para convertir una idea de negócio en empresa**

## **Presentación**

### **Preparando el ambiente**

Bienvenidos y bienvenidas al curso Lean Startup: el método eficaz para transformar una idea de negocio en una empresa. Durante este curso iremos aprender, de forma práctica, los conceptos creados por Eric Reis. Este es un curso rápido que nos ayudará a dar los primeros pasos de este método que tiene como objetivo reducir el desperdicio y aumentar la frecuencia con clientes reales, lo que nos garantiza que las hipótesis del modelo de negocios serán validadas lo más rápido posible. ¡Excelente curso!

---

## **Experimentación rápida y aprendizaje validado**

### **Lean Startup**

¿Cuál es el objetivo principal de Lean Startup?

Rta.

Desarrollar productos y mercados con agilidad. Lean Startup tiene como objetivo reducir el tiempo que dedica a crear un negocio o crear un producto innovador para un público objetivo.

### **Early adopters**

¿Cuál de las siguientes opciones es verdadera sobre los Early Adopters?

Early Adopters son usuarios beta, aquellas personas que entienden que existe un problema y buscan activamente una solución. Los Early Adopters son los primeros usuarios que prueban su producto. De la experiencia y los comentarios de estos usuarios, obtenemos información para modelar o descontinuar nuestro producto.

### **Ciclo de validación**

¿Cuáles son los pasos en un proceso de validación?

Construya, Mida, Aprenda. Este proceso tiene 3 pasos: construir, medir y aprender.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

**Early Adopters y aprendizaje validado**

Todo el contenido que abordaremos durante el curso fue inspirado en el libro **The Lean Startup** de Eric Ries y trabajaremos con ejemplos prácticos que permitirán una mejor comprensión del método.

El primer ejemplo práctico es _Google Wave_. El método Lean Startup busca reducir el tiempo que estaríamos creando un nuevo negocio, hasta que lleguemos a un producto innovador y que atienda satisfaga las necesidades de un público objetivo.

![Google wave](img/google_wave.png "Google wave")

No sirve de nada crear un producto asombroso, con varias características bien modeladas si no sirve a los clientes, si no encuentra una audiencia. Nuestras soluciones de mercado deben ser vendibles. Google Wave fue una iniciativa lanzada por Google en 2009, la idea de la empresa era reinventar el correo electrónico. Resulta que el correo electrónico se creó hace más de 40 años y ya cubre una serie de necesidades, es decir, la gente no quería que se reinventara esta herramienta.

Hubo una gran promoción de lanzamiento BETA para ese producto. Permitía el envío de correos electrónicos, existía la posibilidad de traducción simultánea de los mismos en varios idiomas, y permitía visualizar la evolución de una conversación. Eran características interesantes, pero el furor se fue apagando. Google pensó que estaba creando algo que todos querían y no era así.

**Early adopters**

Y aquí surge la idea de Lean Startup. El primer concepto que veremos es el de Early Adopters, o primeros usuarios. Google lanzó un producto masivo, pensando que todos querían esta nueva herramienta, cuando en realidad tenía más que ver con el entorno corporativo. La empresa debería haber buscado comentarios de los primeros usuarios antes de lanzarse a gran escala. Al principio hubo 100.000 visitas, luego nadie más pudo acceder y finalmente se suspendió en 2010.

Si encontramos a nuestros primeros usuarios, podremos modelar la solución que estamos desarrollando. Incluyendo averiguar si dichos usuarios están interesados ​​en pagar por la solución. Google Wave era un producto masivo, pero este público no se adhirió a él. Quizás si el enfoque fuera otro (corporativo, por ejemplo), Google Wave aún estaría disponible.

Antes de lanzar el producto, los primeros usuarios accederán a la solución y la probarían. A partir de esa prueba, podrían impulsar el desarrollo e incluso el enfoque empresarial.

Usemos otro ejemplo para hablar más sobre los primeros usuarios. Imaginemos que estamos desarrollando un software que ayuda a las personas a perder peso conectando entrenadores personales con estudiantes de gimnasia por un precio más bajo de lo esperado en una consulta cara a cara. Una mujer con sobrepeso fue a gimnasios, tomó batidos para adelgazar y probó diferentes dietas, pero aún así no pudo perder peso. Navegando por Internet buscando una solución a su problema, encuentra nuestro producto, que acaba de ser lanzado y está incompleto. Ella es nuestra primera usuaria, es nuestra pionera. Después de usar el sistema, es este usuario quien dirá cuánto pagará por él.

Para explorar nuestro primer usuario y las posibilidades que nos ofrece, existen varias técnicas. No solo podemos modelar el producto y sus soluciones, sino también descubrir su interés en pagar por la solución. Todo esto antes de que todo se volviera un fracaso, como fue el caso de Google Wave.

**Validación**

Todo esto para llegar a la Validación. Es extremadamente necesario que validemos nuestro producto y nuestras hipótesis creadas para el producto. Lean Startup es un proceso de validación de hipótesis.

Si encontramos a nuestro pionero en adopción, debemos validar si está interesado, por ejemplo, en pagar por la solución. Resolvemos esto construyendo una hipótesis como precio. Por ejemplo, podemos poner un valor de $100,00. Cuando tengamos los cinco primeros usuarios, ofrecemos el producto a ese precio y medimos sus respuestas. De los cinco, sólo dos aceptaron. Esta retroalimentación sólo se puede medir si estamos trabajando con métricas. Si la hipótesis inicial era que al menos cuatro acordaron pagar, la respuesta no es muy buena, se debería cambiar el precio. Aprendemos de este proceso: construimos el producto y las hipótesis para ello, medimos las respuestas teniendo primero una métrica establecida y aprendemos de los comentarios. A partir de ahí reconstruimos el negocio.

![Ciclo de validacion](img/ciclo_de_validacion.png "Ciclo de validacion")

Este es el Ciclo de Validación de Lean Startup.

---

## **Fases del MVP**

Producto Minimo Viable (MVP)

¿Qué es un MVP o Producto mínimo viable?

Rta.

Conocimiento validado. El Producto Mínimo Viable es el conocimiento validado en el proceso Lean.

### **Etapas del MVP**

Según Lean, ¿cuál de las siguientes opciones no forma parte de la etapa MVP?

```txt
A) Recepcion
B) Estructurando el producto vendible.
C) Etapa de planificación.
D) Exploración del problema.
```

Rta.

C.

### **Precificación**

En el paso de fijación de precios de la estructuración del producto vendible, cuál de las siguientes opciones es la correcta:

Rta.

Los precios deben ser top-down.

### **Último paso del MVP**

¿Cuál de las siguientes opciones es el último paso en MVP?

Rta.

Validar el modelo de negócio.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

**Etapas del MVP (Producto Mínimo Viable)**

En esta clase nos centraremos en el MVP y veremos sus diversas etapas. Pasaremos de una hipótesis validada a un producto final vendible. No debemos confundir MVP con este producto final. MVP es mucho más simple, MVP es conocimiento validado.

Volviendo al ejemplo de Dropbox, la empresa creó y lanzó un video en Youtube para que la gente se interesara en comprar el producto, solo tenían que hacer clic en el enlace disponible e irían a la página de la empresa. El video y la página son MVP, ya que había personas interesadas incluso antes de que existiera el producto. De esta forma, pudieron ahorrar tiempo: incluso antes de crear un producto validado y vendible, Dropbox ya tenía clientes. Exactamente lo contrario del caso de Google Wave.

Ahora veamos las tres etapas de MVP.

**1. Exploración del problema:**

Hay que plantear el problema que queremos resolver, es decir, ¿de quién es el problema? ¿Quién es esta audiencia? Es necesario crear una persona, el perfil de nuestro público objetivo.

Con esa definición converse con el público objetivo, realice preguntas exploratorias para entender las necesidades y dolores. También es posible comenzar a probar hipótesis e ideas.

A menudo, el público ya da la idea de un producto que resolverá su problema. Otro enfoque interesante es contar una historia con la que el encuestado pueda identificarse.

Después de la conversación, la comprensión del problema y la construcción de una posible solución, viene su validación. A través de pantallas (o videos, o modelos) que simulan el producto, presentamos la solución para que nuestros Early Adopters la aprueben o no.

**2. Estructuración del producto comercializable:**

Hay tres pasos dentro de la fase de estructuración:

**Precios de arriba hacia abajo:** Es hora de vender nuestro producto. La gente puede incluso querer nuestro servicio, pero tendrá que pagar, lo que puede causar impedimentos. Necesitamos encontrar una forma de vender definiendo un modelo de negocio. Ahora veremos cómo lo hacemos para fijar el precio del producto.

Nunca empezamos a fijar precios de abajo hacia arriba. El cliente puede estar dispuesto a pagar más de lo que pensamos. Así que estipulamos un valor alto para, así, negociarlo y el público acomodar ese valor. Siempre de arriba hacia abajo.

**Innovación:** Tendremos competidores. Tanto las empresas que ya existen como las que vendrán. Y un factor que nos diferenciará en el mercado y nos situará por delante es la innovación. ¿Qué podemos ofrecer que nadie haya ofrecido todavía?

**Beta:** Es la primera versión del producto. La versión Beta es la fase de modelado, aquí es donde tienen lugar la mayoría de los cambios. Los primeros usuarios también se denominan "Usuarios Beta" para nosotros, son socios en esta compilación.

**3. Probar y validar:**

La fase de recepción, o concierge, es la validación del modelo de negocio mediante una simulación manual del mismo sin asumir grandes riesgos. Antes de automatizar su negocio, pruebe a sus pioneros utilizando medios de respuesta baratos y rápidos.

---

## **Medir y pivotar**

### **Pivotes**

¿Qué son los pivotes según Lean?

Rta.

Cambios en la esencia de la solución que impactan en el modelo de negocio.

Tipos de pivotes

¿Cuál de las siguientes opciones no es un tipo de pivote?

```txt
A) Segmento de clientes
B) Zoom in
C) Validacion
```

Rta.

Validación.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

**Medir y pivotar**

Cuando lanzamos nuestro MVP es importante medir los resultados para tomar decisiones. Eric Ries explica que existen dos tipos de métricas: Métricas accionables vs Métricas de vanidad.

Si un sitio web recibe diariamente miles de visitas, esta métrica pude indicarnos resultados positivos. Sin embargo, otras métricas indican que pocas personas permanecen, retornan o compran, lo que impacta directamente nuestro modelo de negocio. La primera métrica es de vanidad, la segunda nos permite entender el problema y tomar decisiones para nuestro negocio.

**Pivotes**

Los pivotes son cambios en la esencia de la solución que impactan en el modelo de negocio.

Si decidimos vender un producto y nos damos cuenta de que no es posible venderlo a un precio alto, pero por debajo de las expectativas, esto significa que necesitaremos trabajar más volumen y precios bajos. O sea, necesitaremos "pivotar" el modelo y la solución, iniciando así un nuevo ciclo iterativo. En resumen: pivote es cada cambio que necesitaremos hacer en nuestro MVP. A partir de pivotar, aparece un nuevo MVP.

**Veamos algunos tipos de pivotes:**

**Zoom in y zoom out**: Son pivotes que comprimen (adentro) o expanden (alejan) una empresa. Imaginemos un proyecto de producto que tiene varias características y sus desarrolladores notan que será mejor enfocarse en solo una de ellas. Hicieron un Zoom-in y convirtieron esta característica en el producto en sí. Un buen ejemplo es el caso de Flickr que, a partir de un juego con función para compartir imágenes, se modificó para ofrecer solo esta función. Zoom-out es lo contrario: nos dimos cuenta de que será necesario agregar más funciones al producto, expandimos el negocio. Un ejemplo de Zoom-out es el caso de GetResponse, que comenzó como un servicio de correo electrónico y hoy en día tiene muchas otras características.

**Segmentación**: Ofrecemos el mismo producto a todos los clientes, pero dándoles opciones. Un buen ejemplo, que puede considerarse el primer caso pivotante de la historia, es la opción de diferentes colores para los coches que ofrece Chevrolet.

Además de esos do también existen pivotes en:

    Plataforma / Producto
    Arquitectura de negocios
    Valor de captura
    Motor de Crecimiento
    Canal
    Tecnología

---

## **Acelerar, crecer e innovar**

### **Métricas**

¿Cuál es el tipo de métricas que Eric Ries alerta en el método Lean Startup?

Rta.

Métricas de vanidad. Las métricas de vanidad que nos muestran números positivos superficiales deben ser evitadas. Las métricas accionables nos permiten entender el problema y tomar decisiones para nuestro negocio.

### **Agrupar en lotes**

¿Por qué es recomendado agrupar en lotes el desarrollo?

Rta.

Lotes pequeños entregan un producto con más agilidad y permiten identificar y corregir problemas rapidamente. Lotes grandes demoran en entregar el producto e identificar posibles problemas.

### **Motores de crecimiento**

¿Cuál de las siguientes opciones no es un motor de crecimiento?

```txt
A) Orgânico.
B) Viral.
C) Recorrente.
D) Pago.
```

Rta.

A.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

**Acelerar, Crecer e innovar**

¿Cómo una startup puede crecer sin sacrificar su velocidad y agilidad que son parte de su ADN? Siguen algunas recomendaciones para responder a esta pregunta.

**Agrupar en lotes**

En un trabajo orientado a proceso, el desempeño individual nos es tan importante como el desempeño del sistema. Aunque la cantidad de tiempo sea exactamente igual, lotes pequeños producen un producto acabado cada pocos segundos, mientras que lotes grandes entregan todos los productos al final. Si tenemos algún problema en el proceso, el modelo de lotes pequeños nos permitirá identificar y corregir el problema rápidamente.

**Ejemplo Toyota:**

- Fue capaz de producir automóviles completos mediante lotes pequeños
- Mejoró el sistema de cambio que llevaba horas para minutos
- Más diversidad de productos
- Logró atender mercados menores que eran más fragmentados
- Sistema Andon permite identificar y corregir problemas con agilidad
- Evaluaciones históricas de alta calidad y de bajo coste de la empresa

Este ejemplo nos muestra que es importante realizar pequeñas mejoras, pero constantes.

**Los 3 motores de crecimiento**

- Recorrente: Pago constante por subscripción. Ejemplo: Netflix, Amazon Prime, TAQE
- Viral: Sistema de recomendación de personas. Ejemplo: Hotmail y el mensaje en los e-mails para crear correo
- Pago: Inversión fuerte en publicidad y equipo de ventas

Es importante identificar cuáles de esos motores son los que influencian en el crecimiento de la empresa para trabajar estrategias adecuadas.

---

## **Recomendaciones para prototipar**

### **Hipótesis de negocio**

¿Cómo validar una hipótesis de negocio?

```txt
A) través de una entrevista.
B) Todas las respuestas son correctas.
C) A través de un formulario enviado por correo electrónico.
D) A través de una página de destino.
```

Rta.

B.

### **Prueba A/B**

¿Cuál es el propósito de realizar una prueba A/B?

Rta.

Comparar el rendimiento de dos o más versiones de una página o elemento específico.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

**Recomendaciones para MVP**

En clases anteriores dimos un ejemplo del éxito de Dropbox y cómo logró atraer a muchos usuarios con un solo video y un enlace. Este enlace conducía a una página de destino, que tiene como objetivo atraer la atención del usuario y venderle el producto o servicio.

Con una Landing Page podemos validar nuestra hipótesis, el primer paso en MVP. De esa forma empezamos a averiguar si hay mercado. Para ello también es necesario establecer métricas como: "de cada 100 visitantes al sitio, queremos convertir 10". Pero, ¿qué significa convertirse? En el caso de Dropbox, los visitantes se suscribieron.

![Lo que aprendimos](img/lo_que_aprendimos.png "Lo que aprendimos")

Esta será la métrica de éxito (o fracaso) y a partir de ahí podemos finalizar el ciclo de validación y tener el primer MVP. En otras palabras, es válido utilizar Landing Page para validar las hipótesis.

Para crear una página web es posible usar herramientas como Bootstrap. Wordpress e Wix.

Podemos monitorizar los accesos y conversiones de usuarios en LP a través de Google Analytics (MEAS) y validar nuestras hipótesis. También es interesante crear más de una página, cada uno con un enfoque diferente, para analizar cuál de ellos tuvo más conversiones (A / B Test) e invertir en la mejor solución o pivote (LEARN).

Recordando que la página no se difunde sola. Podemos hacer esto invirtiendo en AdWords, publicando en grupos en las redes sociales. Sin olvidar también que existen otros métodos para medir, cómo formularios, conversaciones, entrevistas y otras herramientas.

Herramientas para prototipado: también recomendo herramientas como Invision o Adobe XD para transformar el diseño de pantallas de la solución en una navegación.

---

## **Finalización**

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

En este curso de Lean Startup buscamos, a través de ejemplos prácticos, presentar una visión general de los conceptos que rodean este método.

Hemos visto casos de fallas, como Google Wave, y éxitos, como Dropbox. También conocimos las etapas de MVP (Producto Mínimo Viable).

Aprendimos que hay varias formas de validar hipótesis, como la Landing Page, el formulario y la entrevista. Pasando por esta validación y el MVP, llegamos a un producto vendible, que tiene mercado.

**¿Por qué vale la pena emprender usando Lean Startup?**

**Ahorro de tiempo**: No podemos trabajar en una solución que nadie comprará y que nos hará empezar de cero.

**Podemos emprender más**: Trabajaremos con productos que tengan mercado.

**Nos obsesionamos por los datos**: Necesitamos lidiar con más datos y menos sentimientos. Buscamos validaciones de datos entre los usuarios y sus experiencias.

**Lean también es para empresas grandes**: El ciclo de validación puede llevarse a empresas más grandes.

**Lean nos da respuestas rápidas**: Cuando validamos, recibimos una respuesta rápida que, a su vez, nos permite actuar rápidamente para mejorar el producto.

Espero que lo hayas disfrutado. ¡Hasta el próximo curso!
