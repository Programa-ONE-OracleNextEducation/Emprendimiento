# **Emprendimiento - De la idea al plan de negocios**

## **Pilares del emprendimiento**

### **Emprendimiento corporativo**

¿Es posible emprender como colaborador de una organización?

Rta.

Sí. Es posible tener actitud emprendedora e, incluso, desarrollar nuevos negocios como colaborador de una organización. Emprender dentro de una empresa ya establecida es conocido como intraemprendimiento. El riesgo es mucho menor y las oportunidades generalmente son más restrictas al, limitadas al core-business de la organización, además de tener que agradar a muchos interesados.

### **Intraemprendimiento**

¿Cuál de las siguientes alternativas no es una característica del intraemprendimiento?

```txt
A) Actuar alineado con el negocio actual (core-business)
B) Representar intereses de un gran conjunto de interesados (stakeholders)
C) Asumir riesgos mayores
```

Rta.

C. El intraemprendedor(a) asume riesgos menores que el emprendedor(a) porque, generalmente, no coloca su propio capital en riesgo.

### **Mindset Empreendedor**

Vimos que el emprendedor(a) es determinado y proactivo, ¿Cuál de las siguientes características representa el mindset emprendedor?

Rta.

El emprendedor(a) es consciente de que no lo sabe todo y asume que siempre está aprendiendo sobre el mercado y sus clientes. El mindset emprendedor asume que cometer errores es parte del proceso de descubrimiento y que aprender de estos errores es esencial para mejorar el negocio.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Características y mentalidad del emprendedor(a), como determinación, perseverancia, orientación a objetivos, tolerancia al riesgo, entre otras.
- Características que no son adecuadas para la mentalidad de un emprendedor: perfeccionismo, inflexibilidad, creer que sabe todo.
- Las 3 fuerzas del negocio: emprendedores(as), oportunidades y recursos.
- Diferencia de un negocio lifestyle y una startup.
- Es posible ser un intraemprendedor dentro de una organización pero tiene más limitaciones. Es necesario orquestar los intereses de varias personas y considerar el core-business. En contrapartida, tiene menos riesgos.
- Guía para validar nuestro emprendimiento: ¿Mis metas están definidas?,¿Mi estrategia está correcta? ¿Puedo ejecutar la estrategia?

---

## **Ideas y oportunidades**

### **Escalabilidad**

Vimos en clase que las empresas altamente escalables son aquellas que pueden llegar a un gran número de clientes y, si bien aumentan los ingresos, el gasto no aumenta con la misma intensidad. Un buen ejemplo de una empresa altamente escalable es:

Rta.

Cursos online. Así es, las empresas escalables son aquellas que pueden llegar a un gran número de clientes y, si bien aumentan los ingresos, el gasto no aumenta con la misma intensidad. Los cursos en línea se registran solo una vez y luego se pueden vender miles de veces a diferentes clientes, sin tener que volver a registrarse.

### **Posicionamento**

El mejor ejemplo de posicionamiento en el mercado es

Rta.

Buscar atender un segmento del mercado con excelencia, diferenciando su producto / servicio de los demás del mercado. Para diferenciarse de la competencia es necesario segmentar el público y conocer un perfil de cliente con excelencia

### **Atractividad**

Vimos en clase que una de las cosas que diferencia una idea de una oportunidad es el tamaño del dolor del cliente, porque

```txt
A) Es más fácil vender una solución a un problema que ya molesta a tu cliente potencial, que ofrecer una solución para mejorar algo que no te molesta.
B) El cliente siempre estará más dispuesto a pagar por soluciones que resuelvan un problema real, que por aquellas que son simplemente interesantes o le hacen la vida un poco mejor.
C) Cuanto mayor sea el dolor del cliente en relación con el problema que la empresa se propone resolver, más necesita un "remedio" y más atractiva será tu solución.
D) Todas las alternativas están correctas
```

Rta.

D. Los productos o servicios que resuelven problemas reales que molestan a personas y empresas tienen más probabilidades de éxito que aquellos que simplemente son interesantes.

---

## **Modelo de negocios**

### **Complemento**

Vimos en la clase que es posible incrementar la rentabilidad del negocio, agregando complementos a un producto o servicio, como

Rta.

Videojuegos y juegos; Impresoras y cartuchos; Clase de pintura y pinturas.

Quien compra un videojuego, necesita comprar juegos. Cualquiera que compre una impresora necesita comprar cartuchos para poder imprimir.

Los productos o servicios complementarios son aquellos que se pueden vender junto con el producto o servicio original, mejorando la experiencia del cliente y generando nuevas fuentes de ingresos para el negocio.

### **Obstáculos**

Vimos en la clase que algunas características del producto o servicio hacen inviable la adopción por parte del cliente. Un ejemplo de esto es:

Rta.

El cambio cuesta, por ejemplo, si un cliente tiene un teléfono Apple y cambia a Android, pierde las aplicaciones que compró, lo que había guardado en la nube y las canciones que tenía en iTunes. A menudo, el costo de cambiar de un producto a otro, o de un proveedor de servicios a otro, puede ser muy alto y termina haciendo que el negocio sea inviable.

### **Operaciones**

Vimos en clase que desde el punto de vista de las operaciones de la organización, para cada actividad podemos

Rta.

Decidir si vamos a hacer InHouse o Outsourcing, es decir, si lo vamos a externalizar a otras empresas o lo haremos con nuestro propio equipo. Es importante que las actividades más estratégicas que diferencian su negocio de los demás se realicen InHouse.

Es importante considerar cada una de las actividades y subcontratar aquellas que sean estratégicas, o que no generen diferencial para el negocio. La subcontratación de parte de las actividades permitirá al emprendedor invertir más tiempo y energía en lo que hará que el negocio sea más exitoso.

No existe una fórmula mágica, pero en general, se recomienda externalizar lo que no es un diferenciador en tu empresa y quedarte en casa lo que hace que la solución sea única en el mercado.

---

## **Business Model Canvas**

### **Business Model Canvas**

El BMC es:

Rta.

Una forma clara y estructurada de construir nuestra idea de negocios. El BMC muestra tu idea de negocio de una forma clara y estructurada con la ventaja de que otros puedan entender y discutir rápidamente tu modelo de negocio. El BMC ofrece una visión general de tu idea de negocio desde cualquier perspectiva.

### **Lo que aprendimos en aula**

Lo que aprendimos en esta aula:

- Materializar nuestra idea de negocio en un modelo canvas, que resume en un único framework, todos los elementos claves para colocar nuestra empresa en práctica: propuesta de valor, canales, relación con clientes, segmentos, actividades y recursos principales, estructura de costes, fuentes de ingresos y alianzas clave.
- También construimos la propuesta de valor a partir de un modelo Canvas que analiza el perfil del consumidor(a) con sus necesidades, alegrías y frustraciones, y el mapa de valor de nuestra solución, que incluye productos y servicios, creadores de alegrías y aliviadores de frustraciones.

---

## **Plan de negocios**

### **Beneficios de hacer un plan de negocios**

Vimos en clase que, a pesar de las críticas, hay beneficios en hacer un plan de negocios. ¿Por qué?

El plan de negocio ayuda a hacer tangible cómo poner en práctica tu idea, para aumentar todos los costos involucrados. El plan de negocios puede ser muy útil porque hace que el emprendedor ponga ciertas cosas en papel, y haga cuentas importantes para aumentar las posibilidades de éxito empresarial. Aunque es poco probable que los pronósticos a 5 años sucedan de la misma manera que se planeó, el ejercicio será útil para delinear un plan de acción que podría cambiar y revisarse con el tiempo.

### **Riesgos**

En relación a las premisas de evaluación de riesgos asociados a la elaboración de planes de negocio, señale entre las alternativas a continuación la única verdadera que deberíamos intentar seguir.

Rta.

Examine los principales riesgos conocidos, encuentre el mejor y el peor escenario para cada situación y cree planes de contingencia para cada uno.

Estudiar los riesgos conocidos y pensar en planes para mitigar y hacer frente a los problemas en caso de que ocurran, puede marcar una gran diferencia para el emprendedor(a) y garantizar que el negocio no se quiebre en el primer viaje.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Construir un plan de negocios más detallado es importante para ejercitar nuestra planificación y puede ser solicitado por inversionistas interesados en nuestra empresa.
- Pensar en un plazo de 5 años puede permitirnos identificar posibles dificultades y mostrar nuestra capacidad de planeación y ejecución para los inversionistas.
- El plano debe ser evaluado constantemente. No puede quedarse parado, pues el escenario, el mercado y la realidad de la empresa cambian constantemente.
- Existen 9 elementos fundamentales para un plan de negocios: resumen ejecutivo, producto o servicio, equipo, mercado, marketing y ventas, organización, cronograma, riesgos y finanzas.
- Mostramos un ejemplo de planilla para proyectar el estado de resultados.
