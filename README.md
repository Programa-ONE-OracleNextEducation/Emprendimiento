# Emprendimiento

## **Habilidades para el mercado moderno**

Las habilidades para el mercado de tecnología no es solo programación, también incluye organización personal, trabajo en equipo, habilidades de emprendimiento, etc.

En este plan de estudios, verás las habilidades esenciales necesarias para ser un profesional existoso.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Priscila Stuani
- Ana María Correa Rodríguez
- Daniel Gonzalez

## **Paso a paso**

### **1. Contenido**

[Lean Startup: Método eficaz para convertir una idea de negócio en empresa](https://app.aluracursos.com/course/lean-startup-metodo-eficaz-idea-negocio-empresa "Lean Startup: Método eficaz para convertir una idea de negócio en empresa"). Contenido [aqui](./Docs/lean_startup.md).

[Emprendimiento: De la idea al plan de negocios](https://app.aluracursos.com/course/emprendimiento-idea-plan-negocios "Emprendimiento: De la idea al plan de negocios"). Contenido [aqui](./Docs/emprendimiento.md).

[Pitch para entrevistas: Haga presentaciones impactantes](https://app.aluracursos.com/course/pitch-entrevistas-presentaciones-impactantes "Pitch para entrevistas: Haga presentaciones impactantes"). Contenido [aqui](./Docs/pitch_para_entrevistas.md).

[Elevator pitch: aprende a usar este eficiente método de presentación](https://www.aluracursos.com/blog/elevator-pitch-aprenda-a-utilizar-el-metodo-eficaz-de-presentacion "Alura LATAM").

[Business Model Canvas parte I: Un modelo poderoso para tu negócio](https://app.aluracursos.com/course/business-model-canvas-parte-1-modelo-poderoso-negocio "Business Model Canvas parte I: Un modelo poderoso para tu negócio"). Contenido [aqui](./Docs/business_model_canvas_parte_i.md).

[Business Model Canvas parte II: Avanzando con tu modelo de negocios](https://app.aluracursos.com/course/business-model-canvas-parte-2-modelo-negocios "Business Model Canvas parte II: Avanzando con tu modelo de negocios"). Contenido [aqui](./Docs/business_model_canvas_parte_ii.md).

### **2. Diagnóstico de lo aprendido**

Este paso es obligatorio Cuéntanos sobre tus conocimientos adquiridos hasta el momento. Es un diagnóstico personal y por ello es muy importante que seas sincero con tus respuestas.

[Ruta Empren.](https://grupocaelum.typeform.com/to/ZxyGGnH1 "Ruta Empren.").
